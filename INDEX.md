# Fdisk

Fixed disk tool - create partitions.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FDISK.LSM

<table>
<tr><td>title</td><td>Fdisk</td></tr>
<tr><td>version</td><td>1.3.4a</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-02-22</td></tr>
<tr><td>description</td><td>Fixed disk tool - create partitions.</td></tr>
<tr><td>keywords</td><td>fdisk, format, hard drive, partition, fat16, fat32, partition, mbr</td></tr>
<tr><td>author</td><td>Brian E. Reifsnyder &lt;breifsnyde -at- state.pa.us&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/fdisk</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, version 2](LICENSE)</td></tr>
</table>
